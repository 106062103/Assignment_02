var bootState = {
  preload:function(){
    game.load.image('progressBar','asset/progressBar1.png');
  },

  create:function(){
    game.stage.backgroundColor = '#696969';
    game.physics.startSystem(Phaser.Physics.ARCADE);
    game.state.start('load');
  }
};
