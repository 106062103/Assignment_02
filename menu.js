var menuState = {
  preload:function(){

  },

  create:function(){
    this.back = game.add.tileSprite(0,0,game.width,game.height,'sea');
    var Bartext = game.add.text(game.width/2,150,'Raiden',{font:'72px Arial',fill:"#ffffff"});
    Bartext.anchor.setTo(0.5,0.5);
    var gamestart = game.add.text(game.width/2,250,'Game Start',{font:'30px Arial',fill:'#ffffff'});
    gamestart.anchor.setTo(0.5,0.5);
    var leaderboard = game.add.text(game.width/2,300,'Leader Board',{font:'30px Arial',fill:'#ffffff'});
    leaderboard.anchor.setTo(0.5,0.5);
    var info = game.add.text(game.width/2,350,'Control',{font:'30px Arial',fill:'#ffffff'});
    info.anchor.setTo(0.5,0.5);
    var littletext = game.add.text(game.width/2,400,'Press Z to choose',{font:'20px Arial',fill:"#ffffff"})
    littletext.anchor.setTo(0.5,0.5);
    this.littletextTween = game.add.tween(littletext).to({alpha:0},500).yoyo(true);
    this.littletextTween.start().repeat();
    this.key = game.input.keyboard.createCursorKeys();
    this.selectL = game.add.image(game.width/2-120,248,'choice1');
    this.selectR = game.add.image(game.width/2+120,248,'choice2');
    this.selectL.anchor.setTo(0.5,0.5);
    this.selectR.anchor.setTo(0.5,0.5);
    this.choice = 1;
    this.keyZ = game.input.keyboard.addKey(Phaser.Keyboard.Z);
    if(game.global.bgm == null){
      game.global.bgm = game.add.audio('bgm');
      game.global.bgm.loop = true;
      game.global.bgm.allowMultiple = false;
      game.global.bgm.volume = 0.5;
    }
    console.log(game.global.bgm.isPlaying);
    if(!game.global.bgm.isPlaying){
      game.global.bgm.play();
    }
  },

  update:function(){
    this.back.tilePosition.y += 1;
    this.key.up.onDown.add(this.Up,this);
    this.key.down.onDown.add(this.Down,this);
    this.keyZ.onDown.add(this.enter,this);
  },
  enter:function(){
    if(this.choice == 1){
      game.state.start('main');
    }
    else if(this.choice == 2){
      game.state.start('leader');
    }
    else if(this.choice == 3){
      game.state.start('control');
    }
  },

  Up:function(){
    if(this.choice == 1){
      this.choice = 3;
    }
    else{
      this.choice--;
    }
    this.choose();
  },

  Down:function(){
    if(this.choice == 3){
      this.choice = 1;
    }
    else{
      this.choice++;
    }
    this.choose();
  },

  choose:function(){
    if(this.choice == 1){
      this.selectL.kill();
      this.selectR.kill();
      this.selectL.reset(game.width/2-120,248);
      this.selectR.reset(game.width/2+120,248);
    }
    else if(this.choice == 2){
      this.selectL.kill();
      this.selectR.kill();
      this.selectL.reset(game.width/2-120,298);
      this.selectR.reset(game.width/2+120,298);
    }
    else if(this.choice == 3){
      this.selectL.kill();
      this.selectR.kill();
      this.selectL.reset(game.width/2-120,348);
      this.selectR.reset(game.width/2+120,348);
    }
  }

};