const game_width = 600;
const ui_width = 300;
//var score = 0;
var score_display = [];
var life_display = [];
var life = 2;
var level = 1;
var stage = 1;
var boss_reset = false;
var Boss;
var work = true;
var mainstate = {
  preload:function(){
    /*
    game.load.spritesheet('number','asset/number.png',32,40);
    game.load.spritesheet('player','asset/pipo-airship02.png',32,32);
    game.load.spritesheet('enemy','asset/pipo-airship01.png',32,32);
    game.load.spritesheet('inivincible_img','asset/Flash.png',64,64);
    game.load.spritesheet('boss','asset/boss.png',96,92);
    game.load.spritesheet('flash','asset/skill_all.png',64,64);
    game.load.spritesheet('Ult','asset/ultimateQAQ.png',64,64);
    game.load.spritesheet('exp','asset/explosion1.png',240,240);
    game.load.spritesheet('eneExp','asset/eneExp.png',40,40);
    game.load.image('bullet','asset/bullet.png');
    game.load.image('bullet_enemy','asset/bullet_enemy.png');
    game.load.image('sea','asset/sea.png');
    game.load.image('heart','asset/sticon3b-3.png');
    game.load.image('darkHeart','asset/sticon3j-3.png');
    game.load.image('bomb','asset/bullet10.png');
    */
  },

  create:function(){
    //init
    level = 1;
    stage = 1;
    life = 2;
    game.global.score = 0;
    game.global.FightBoss = false;
    boss_reset = false;
    work = true;
    //game.stage.backgroundColor = '#696969';
    this.sea = game.add.tileSprite(0,0,game_width,500,'sea');
    this.scoreTxt = game.add.text(game_width,0,'Score : ',{font:'34px Arial',fill:'#fff'});
    this.lifeTxt = game.add.text(game_width,50,'Life : ',{font:'34px Arial',fill:'#fff'});
    for(i=0;i<3;i++){
      life_display[i] = game.add.image(game_width + 80 + i*26,60,'heart');
    }
    game.world.setBounds(0,0,game_width-5,500);
    //game.physics.startSystem(Phaser.Physics.ARCADE);
    //keyboard
    this.cursor = game.input.keyboard.createCursorKeys();
    //player
    this.player = game.add.sprite(game_width/2,game.height,'player');
    this.player.anchor.setTo(0.5,0.5);
    this.player.animations.add('normal',[0,1],8,true);
    
    game.physics.arcade.enable(this.player);
    this.player.body.collideWorldBounds = true;
    this.player.body.setSize(23,32,4,0);
    //drop score
    this.dropScore = game.add.group();
    this.dropScore.enableBody = true;
    this.dropScore.createMultiple(100,'coin');
    this.dropScore.setAll('anchor.x',0.5);
    this.dropScore.setAll('anchor.y',0.5);
    this.dropScore.setAll('outOfBoundsKill',true);
    this.dropScore.setAll('checkWorldBounds',true);
    //drop heart
    this.dropHeart = game.add.group();
    this.dropHeart.enableBody = true;
    this.dropHeart.createMultiple(100,'dropHeart');
    this.dropHeart.setAll('anchor.x',0.5);
    this.dropHeart.setAll('anchor.y',0.5);
    this.dropHeart.setAll('outOfBoundsKill',true);
    this.dropHeart.setAll('checkWorldBounds',true);
    //drop aim
    this.dropAim = game.add.group();
    this.dropAim.enableBody = true;
    this.dropAim.createMultiple(100,'aim');
    this.dropAim.setAll('anchor.x',0.5);
    this.dropAim.setAll('anchor.y',0.5);
    this.dropAim.setAll('outOfBoundsKill',true);
    this.dropAim.setAll('checkWorldBounds',true);
    //drop special
    this.dropSpecial = game.add.group();
    this.dropSpecial.enableBody = true;
    this.dropSpecial.createMultiple(100,'special');
    this.dropSpecial.setAll('anchor.x',0.5);
    this.dropSpecial.setAll('anchor.y',0.5);
    this.dropSpecial.setAll('outOfBoundsKill',true);
    this.dropSpecial.setAll('checkWorldBounds',true);
    //aiming bullet
    this.aim = game.add.group();
    this.aim.enableBody = true;
    this.aim.createMultiple(100,'aimbullet');
    this.aim.setAll('anchor.x',0.5);
    this.aim.setAll('anchor.y',0.5);
    this.aim.setAll('outOfBoundsKill',true);
    this.aim.setAll('checkWorldBounds',true);
    this.aimlevel = 0;
    this.aimshotCD = 0;
    this.aimshotDelay1 = 2000;
    this.aimshotDelay2 = 1500;
    //bullets
    this.bullets = game.add.group();
    this.bullets.enableBody = true;
    this.bullets.createMultiple(100,'bullet');
    this.bullets.setAll('anchor.x',0.5);
    this.bullets.setAll('anchor.y',0.5);
    this.bullets.setAll('outOfBoundsKill', true);
    this.bullets.setAll('checkWorldBounds', true);
    this.nextShot = 0;
    this.shotDelay = 100; 
    this.shotAudio = game.add.audio('shoot');
    this.shotAudio.volume = 0.5;
    //bullets_enemy
    this.bullet_enemy = game.add.group();
    this.bullet_enemy.enableBody = true;
    this.bullet_enemy.createMultiple(100,'bullet_enemy');
    this.bullet_enemy.setAll('anchor.x',0.5);
    this.bullet_enemy.setAll('anchor.y',0.5);
    this.bullet_enemy.setAll('outOfBoundsKill',true);
    this.bullet_enemy.setAll('checkWorldBounds',true);
    //enemies
    this.enemies = game.add.group();
    this.enemies.enableBody = true;
    this.enemies.createMultiple(50,'enemy');
    this.enemies.setAll('anchor.x',0.5);
    this.enemies.setAll('anchor.y',0.5);
    this.enemies.setAll('outOfBoundsKill',true);
    this.enemies.setAll('checkWorldBounds',true);
    this.enemies.setAll('body.width',23);
    this.enemies.setAll('body.offset.x',4);
    this.enemies.setAll('drop',0);
    this.enemies.forEach(enemy => {
      enemy.animations.add('move',[0,1],8,true);
    });
    this.nextEnemy = 0;//stage1
    this.enemyDelay = 500;//stage1
    this.enemyStage2 = 0;
    this.enemyStage2Delay = 700;
    //boss
    this.boss = game.add.group();
    this.boss.enableBody = true;
    this.boss.createMultiple(1,'boss');
    this.boss.setAll('anchor.x',0.5);
    this.boss.setAll('anchor.y',0.5);
    this.boss.setAll('checkWorldBounds',true);
    this.boss.setAll('body.collideWorldBounds',true);
    this.boss.forEach(theBoss=>{
      theBoss.animations.add('bossAnimation',[0,1,2],4,true);
    })
    this.boss.way = true;
    this.boss.moveCD = 0;
    this.boss.moveDelay = 7000;
    this.boss.fireCD = 0;
    this.boss.fireDelay = 500;
    this.boss.Hp = 1000;
    //skill-flash
    this.Flash = game.add.sprite(game_width+20,game.height-90,'flash');
    this.Flash.animations.add('skill_flash',[1,2,3,4],1,false);
    //skill-Invincible
    this.Invincible = game.add.sprite(game_width+94,game.height-90,'inivincible_img');
    this.Invincible.animations.add('inivincible',[1,2,3,4,5,6,7,8,9],1,false);
    this.invincibleTween = game.add.tween(this.player).to({alpha:0},250).yoyo(true);
    //skill-ultimate
    this.Ultimate = game.add.sprite(game_width+168,game.height-90,'Ult');
    this.Ultimate.animations.add('Ultimate',[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18],1,false);
    this.Bomb =game.add.group();
    this.Bomb.enableBody = true;
    this.Bomb.createMultiple(1,'bomb');
    this.Bomb.setAll('anchor.x',0.5);
    this.Bomb.setAll('anchor.y',0.5);
    this.Bomb.setAll('checkWorldBounds',true);
    this.Bomb.setAll('outOfBoundsKill',true);
    this.bomb;
    //boss emitter
    this.emitter = game.add.emitter(0, 0,15);
    this.emitter.makeParticles('pixel');
    this.emitter.setYSpeed(-150,150);
    this.emitter.setXSpeed(-150,150);
    this.emitter.setScale(2,0,2,0,800);
    this.emitter.gravity = 0;
    //special
    this.helper1 = game.add.image(0,0,'helper');
    this.helper1.anchor.setTo(0.5,0.5);
    this.helper1.kill();
    this.helper2 = game.add.image(0,0,'helper');
    this.helper2.anchor.setTo(0.5,0.5);
    this.helper2.kill();
    this.GetHelper = false;
    this.helperTime = 0;
    this.helperDur = 5000;

    this.raser1 = game.add.sprite(0,0,'raser');
    this.raser1.anchor.setTo(0.5,1);
    game.physics.arcade.enable(this.raser1);
    this.raser1.checkWorldBounds = true;
    this.raser2 = game.add.sprite(0,0,'raser');
    this.raser2.anchor.setTo(0.5,1);
    game.physics.arcade.enable(this.raser2);
    this.raser2.checkWorldBounds = true;


    this.keyV = game.input.keyboard.addKey(Phaser.Keyboard.V);
    this.keyP = game.input.keyboard.addKey(Phaser.Keyboard.P);
    this.keyQ = game.input.keyboard.addKey(Phaser.Keyboard.Q);
    this.Qbutton = game.add.image(game_width+20,game.height/2-50,'Q_up');
    this.keyW = game.input.keyboard.addKey(Phaser.Keyboard.W);
    this.Wbutton = game.add.image(game_width+160,game.height/2-50,'W_down');
    this.keyE = game.input.keyboard.addKey(Phaser.Keyboard.E);
    this.Ebutton = game.add.image(game_width+220,game.height/2-50,'E');
    this.shotText = game.add.text(game_width+105,game.height/2-25,'Voice',{font:'30px Arial',fill:"#000000"});
    this.shotText.anchor.setTo(0.5,0.5);

    this.keyA = game.input.keyboard.addKey(Phaser.Keyboard.A);
    this.Abutton = game.add.image(game_width+20,game.height/2+20,'A_up');
    this.keyS = game.input.keyboard.addKey(Phaser.Keyboard.S);
    this.Sbutton = game.add.image(game_width+160,game.height/2+20,'S_down');
    this.keyD = game.input.keyboard.addKey(Phaser.Keyboard.D);
    this.Dbutton = game.add.image(game_width+220,game.height/2+20,'D');
    this.BGMText = game.add.text(game_width+105,game.height/2+45,'BGM',{font:'30px Arial',fill:"#000000"});
    this.BGMText.anchor.setTo(0.5,0.5);

    this.keyVFlag = false;
    this.player.animations.play('normal');
    this.changeScore();
  },

  update:function(){
    if(work){
      //bullets collide enemies
      game.physics.arcade.overlap(this.bullets,this.enemies,this.bulletsHitEnemies,null,this); 
      game.physics.arcade.collide(this.bullets,this.enemies);
      //aim collide enemies
      game.physics.arcade.overlap(this.aim,this.enemies,this.bulletsHitEnemies,null,this);
      game.physics.arcade.collide(this.aim,this.enemies);
      //dropHeart collide player
      game.physics.arcade.overlap(this.dropHeart,this.player,this.eatHeart,null,this);
      game.physics.arcade.collide(this.dropHeart,this.player);
      //dropAim collide player
      game.physics.arcade.overlap(this.dropAim,this.player,this.eatAim,null,this);
      game.physics.arcade.collide(this.dropAim,this.player);
      //dropScore collide player
      game.physics.arcade.overlap(this.dropScore,this.player,this.eatScore,null,this);
      game.physics.arcade.collide(this.dropScore,this.player);
      //dropSpecial collide player
      game.physics.arcade.overlap(this.dropSpecial,this.player,this.eatSpecial,null,this);
      game.physics.arcade.collide(this.dropSpecial,this.player);
      //raser collide bullet
      game.physics.arcade.overlap(this.raser2,this.bullet_enemy,this.bullet_enemyHitBullets,null,this);
      game.physics.arcade.collide(this.raser2,this.bullet_enemy)
      game.physics.arcade.overlap(this.raser1,this.bullet_enemy,this.bullet_enemyHitBullets,null,this);
      game.physics.arcade.collide(this.raser1,this.bullet_enemy)
      //raser collide enemy
      game.physics.arcade.overlap(this.raser1,this.enemies,this.raserHitEnemies,null,this); 
      game.physics.arcade.collide(this.raser1,this.enemies);
      game.physics.arcade.overlap(this.raser2,this.enemies,this.raserHitEnemies,null,this); 
      game.physics.arcade.collide(this.raser2,this.enemies);
      //bullets collide Boss
      if(stage == 3){
        game.physics.arcade.overlap(this.bullets,Boss,this.bulletsHitBoss,null,this); 
        game.physics.arcade.collide(this.bullets,Boss);
        game.physics.arcade.overlap(this.aim,Boss,this.bulletsHitBoss,null,this); 
        game.physics.arcade.collide(this.aim,Boss);
        //raser
        game.physics.arcade.overlap(this.raser1,Boss,this.raserHitBoss,null,this); 
        game.physics.arcade.collide(this.raser1,Boss);
        game.physics.arcade.overlap(this.raser2,Boss,this.raserHitBoss,null,this); 
        game.physics.arcade.collide(this.raser2,Boss);
      }
      //enemybullets collide bullets
     /* game.physics.arcade.overlap(this.bullet_enemy,this.bullets,this.bullet_enemyHitBullets,null,this);
      game.physics.arcade.collide(this.bullet_enemy,this.bullets);*/
      //player collide enemies
      if(!this.invincibleTween.isRunning){
        game.physics.arcade.overlap(this.player,this.enemies,this.playerHitEnemies,null,this);
        game.physics.arcade.collide(this.player,this.enemies);
      }
      //enemybullets collide player
      if(!this.invincibleTween.isRunning){
        game.physics.arcade.overlap(this.bullet_enemy,this.player,this.bullet_enemyHitPlayer,null,this);
        game.physics.arcade.collide(this.bullet_enemy,this.player);
      }
      //boss collide player
      if(!this.invincibleTween.isRunning){
        game.physics.arcade.overlap(Boss,this.player,this.BossHitPlayer,null,this);
        game.physics.arcade.collide(Boss,this.player);
      }
  
      this.sea.tilePosition.y += 1;
      if(stage <3){
        this.createEnemy();
        this.enemyFire();
      }
      else if(stage == 3){
        this.stage3();
        this.bossFire();
      }
      this.keyQ.onDown.add(this.musicUp,this);
      this.keyW.onDown.add(this.musicDown,this);
      this.keyE.onDown.add(this.musicMute,this);
      this.keyA.onDown.add(this.bgmUp,this);
      this.keyS.onDown.add(this.bgmDown,this);
      this.keyD.onDown.add(this.bgmMute,this);
      if(this.GetHelper){
        this.helperComing();
      }
      this.fire();
      this.skill();
      this.aimattack();
      this.autoattack();
      this.movePlayer();

    }
    this.keyP.onDown.add(this.pause,this);
  },
  
  pause:function(){
    if(work){work = false;}
    else{work = true;}
  },

  helperComing:function(){
    if(this.helperTime<game.time.now){
      this.GetHelper = false;
      this.helper1.kill();
      this.helper2.kill();
      this.raser1.kill();
      this.raser2.kill();
      return;
    }
    if(this.helper1.alive){
      this.helper1.x = this.player.x-30;
      this.helper2.x = this.player.x+30;
      this.helper1.y = this.player.y;
      this.helper2.y = this.player.y;
      this.raser1.x = this.helper1.x;
      this.raser2.x = this.helper2.x;
      this.raser1.y = this.helper1.y-15;
      this.raser2.y = this.helper2.y-15;
    }
    else{
      this.helper1.reset(this.player.x-30,this.player.y);
      this.helper2.reset(this.player.x+30,this.player.y);
      this.raser1.reset(this.helper1.x,this.helper1.y-15);
      this.raser2.reset(this.helper2.x,this.helper2.y-15);
    }
  },

  musicDown:function(){
    if(this.shotAudio.volume>0.0){
      this.shotAudio.volume-=0.1;
    }
    else{
      this.shotAudio.volume = 0.0;
    }
  },

  musicUp:function(){
    if(this.shotAudio.volume<1){
      this.shotAudio.volume+=0.1;
    }
    else{
      this.shotAudio.volume = 1;
    }
  },

  musicMute:function(){
    if(this.shotAudio.mute){
      this.Ebutton.kill();
      this.Ebutton = game.add.image(game_width+220,game.height/2-50,'E');
      this.shotAudio.mute = false;
    }
    else{
      this.Ebutton.kill();
      this.Ebutton = game.add.image(game_width+220,game.height/2-50,'E_');
      this.shotAudio.mute = true;
    }
  },

  bgmUp:function(){
    if(game.global.bgm.volume<1){
      game.global.bgm.volume+=0.1;
    }
    else{
      game.global.bgm.volume = 1;
    }
  },

  bgmDown:function(){
    if(game.global.bgm.volume>0.0){
      game.global.bgm.volume-=0.1;
    }
    else{
      game.global.bgm.volume = 0.0;
    }
  },

  bgmMute:function(){
    if(game.global.bgm.mute){
      this.Dbutton.kill();
      this.Dbutton = game.add.image(game_width+220,game.height/2+20,'D');
      game.global.bgm.mute = false;
    }
    else{
      this.Dbutton.kill();
      this.Dbutton = game.add.image(game_width+220,game.height/2+20,'D_');
      game.global.bgm.mute = true;
    }
  },

  eatSpecial:function(special,player){
    player.kill();
    this.helperTime = game.time.now + this.helperDur;
    this.GetHelper = true;
  },

  eatScore:function(score,player){
    player.kill();
    game.global.score += 20;
    this.changeScore();
  },

  eatAim:function(aim,player){
    player.kill();
    if(this.aimlevel<3){
      this.aimlevel++;
    }
  },

  eatHeart:function(heart,player){
    player.kill();
    if(life<2){
      life++;
      life_display[life].kill();
      life_display[life] = game.add.image(game_width + 80 + life*26,60,'heart');
    }
  },
  
  dropItem:function(drop){
    var dropdown = null;
    if(drop<60){

    }
    else if(drop >=60 && drop <70){
      dropdown = this.dropScore.getFirstExists(false);
    }
    else if(drop >=70 && drop <75){
      dropdown = this.dropHeart.getFirstExists(false);
    }
    else if(drop >=80 && drop <85){
      dropdown = this.dropAim.getFirstExists(false);
    }
    else if(drop>=85 && drop<87){
      dropdown = this.dropSpecial.getFirstExists(false);
    }
    return dropdown;
  },

  aimattack:function(){
    var enemy = this.enemies.getFirstAlive();
    if(stage == 1||stage == 2){
      if(this.aimshotCD < game.time.now && enemy){
        switch(this.aimlevel){
          case 1:
            this.aimshotCD = game.time.now + this.aimshotDelay1;
            var rocket = this.aim.getFirstExists(false);
            rocket.reset(this.player.x,this.player.y-20);
          break;
          case 2:
            this.aimshotCD = game.time.now + this.aimshotDelay2;
            var rocket = this.aim.getFirstExists(false);
            rocket.reset(this.player.x,this.player.y-20);
          break;
          case 3:
            this.aimshotCD = game.time.now + this.aimshotDelay2;
            var rocket = this.aim.getFirstExists(false);
            rocket.reset(this.player.x+15,this.player.y-15);
            var rocket1 = this.aim.getFirstExists(false);
            rocket1.reset(this.player.x-15,this.player.y-15);
          break;
        }
      }
    }
    else if(stage == 3){
      if(this.aimshotCD < game.time.now && Boss.alive){
        switch(this.aimlevel){
          case 1:
            this.aimshotCD = game.time.now + this.aimshotDelay1;
            var rocket = this.aim.getFirstExists(false);
            rocket.reset(this.player.x,this.player.y-20);
          break;
          case 2:
            this.aimshotCD = game.time.now + this.aimshotDelay2;
            var rocket = this.aim.getFirstExists(false);
            rocket.reset(this.player.x,this.player.y-20);
          break;
          case 3:
            this.aimshotCD = game.time.now + this.aimshotDelay2;
            var rocket = this.aim.getFirstExists(false);
            rocket.reset(this.player.x+15,this.player.y-15);
            var rocket1 = this.aim.getFirstExists(false);
            rocket1.reset(this.player.x-15,this.player.y-15);
          break;
        }
      }
    }
  },

  autoattack:function(){
    var enemy = this.enemies.getFirstAlive();
    if(stage == 1||stage == 2){
      if(enemy){
        this.aim.forEachAlive(shoot =>{
          enemyy = this.searchEnemy(shoot);
          game.physics.arcade.moveToObject(shoot,enemyy,200);
        })
      }
    }
    else if(stage == 3){
      if(Boss.alive){
        this.aim.forEachAlive(shoot =>{
          game.physics.arcade.moveToObject(shoot,Boss,200);
        })
      }
    }
  },

  searchEnemy:function(shoot){
    var Mindistance = 99999999;
    var enemychoose = null;
    this.enemies.forEachAlive(enemy=>{
      var distance = (shoot.x - enemy.x)*(shoot.x - enemy.x) + (shoot.y - enemy.y)*(shoot.y - enemy.y);
      if(distance<Mindistance){
        Mindistance = distance;
        enemychoose = enemy;
      }
    })
    return enemychoose;
  },

  skill:function(){
    //flash
    if(!this.Flash.animations.currentAnim.isPlaying){
      if(game.input.keyboard.isDown(Phaser.Keyboard.X) && this.cursor.left.isDown){
        this.player.body.x = this.player.body.x-80;
        this.Flash.animations.play('skill_flash');
      }
      if(game.input.keyboard.isDown(Phaser.Keyboard.X) && this.cursor.right.isDown){
        this.player.body.x = this.player.body.x+80;
        this.Flash.animations.play('skill_flash');
      }
      if(game.input.keyboard.isDown(Phaser.Keyboard.X) && this.cursor.up.isDown){
        this.player.body.y = this.player.body.y-80;
        this.Flash.animations.play('skill_flash');
      }
      if(game.input.keyboard.isDown(Phaser.Keyboard.X) && this.cursor.down.isDown){
        this.player.body.y = this.player.body.y+80;
        this.Flash.animations.play('skill_flash');
      }
    }
    if(this.Flash.animations.currentAnim.isFinished){
      this.Flash.frame = 0;
    }
    //invincible
    if(!this.Invincible.animations.currentAnim.isPlaying){
      if(game.input.keyboard.isDown(Phaser.Keyboard.C)){
        this.invincibleTween.start().repeat(9);
        this.Invincible.animations.play('inivincible');
      }
    }
    if(this.Invincible.animations.currentAnim.isFinished){
      this.Invincible.frame = 0;
    }
    //Ult
    if(this.bomb){
      if(this.bomb.alive){
        if(game.input.keyboard.isDown(Phaser.Keyboard.V)){
          this.bomb.body.velocity.y = 0;
          this.bombTween.start();
          this.bombTween.onComplete.add(this.bombKill,this)
        }
      }
    }
    if(!this.Ultimate.animations.currentAnim.isPlaying){
      if(this.keyV.isDown){
        this.keyVFlag = true;  
      }
      if(this.keyV.isUp && this.keyVFlag == true){
        this.keyVFlag = false;
        this.bomb = this.Bomb.getFirstExists(false);
        this.bomb.reset(this.player.x,this.player.y-20);
        this.bomb.body.velocity.y = -100;
        this.bombTween = game.add.tween(this.bomb).to({angle:360},500);
        this.Ultimate.animations.play('Ultimate');
      }
    }
    if(this.Ultimate.animations.currentAnim.isFinished){
      this.Ultimate.frame = 0;
    }
  },

  bombKill:function(){
    var explo = game.add.sprite(this.bomb.x-120,this.bomb.y-120,'exp');
    game.physics.arcade.enable(explo);
    explo.animations.add('explo',[0,1,2,3,4,5,6,7,8,9],24,false);
    this.bomb.kill();
    explo.animations.play('explo',null,false,true);
    
    game.physics.arcade.overlap(explo,this.enemies,this.exploEnemies,null,this); 
    game.physics.arcade.collide(explo,this.enemies);
    game.physics.arcade.overlap(explo,this.bullet_enemy,this.exploBullets,null,this); 
    game.physics.arcade.collide(explo,this.bullet_enemy);
    if(stage == 3){
      game.physics.arcade.overlap(explo,Boss,this.exploBoss,null,this); 
      game.physics.arcade.collide(explo,Boss);
    }

  },

  exploBoss:function(explo,boss){
    this.boss.Hp -= 100;
    if(this.boss.Hp <=0){
      game.global.FightBoss = true;
      this.emitter.x = boss.x;
      this.emitter.y = boss.y;
      this.emitter.start(true,800,null,15);
      boss.kill();
      game.time.events.add(1000,function(){game.state.start('end');});
    }
  },

  exploBullets:function(explo,bullets){
    bullets.kill();
  },

  exploEnemies:function(explo,enemies){
    var item = this.dropItem(enemies.drop);
    enemies.kill();
    if(item){
      item.reset(enemies.x,enemies.y);
      item.body.velocity.y = 100;
    }
    var enemyDie = game.add.sprite(enemies.x-20,enemies.y-20,'eneExp');
    enemyDie.animations.add('Die',[0,1,2,3,4,5],16,false);
    enemyDie.animations.play('Die',null,false,true);
    game.global.score += 10;
    this.changeScore();
  },

  bossFire:function(){
    if(Boss.alive && this.boss.fireCD < game.time.now){
      this.boss.fireCD = game.time.now + this.boss.fireDelay;
      var bossBul1 = this.bullet_enemy.getFirstExists(false);
      bossBul1.reset(Boss.x,Boss.y+20);
      bossBul1.body.velocity.y = 300;
      var bossBul2 = this.bullet_enemy.getFirstExists(false);
      bossBul2.reset(Boss.x,Boss.y-20);
      bossBul2.body.velocity.y = -300;
      var bossBul3 = this.bullet_enemy.getFirstExists(false);
      bossBul3.reset(Boss.x+20,Boss.y);
      bossBul3.body.velocity.x = 300;
      var bossBul4 = this.bullet_enemy.getFirstExists(false);
      bossBul4.reset(Boss.x-20,Boss.y);
      bossBul4.body.velocity.x = -300;
      var bossBul5 = this.bullet_enemy.getFirstExists(false);
      bossBul5.reset(Boss.x+10,Boss.y+10);
      bossBul5.body.velocity.x = 212;
      bossBul5.body.velocity.y = 212;
      var bossBul6 = this.bullet_enemy.getFirstExists(false);
      bossBul6.reset(Boss.x-10,Boss.y+10);
      bossBul6.body.velocity.y = 212;
      bossBul6.body.velocity.x = -212;
      var bossBul7 = this.bullet_enemy.getFirstExists(false);
      bossBul7.reset(Boss.x-10,Boss.y-10);
      bossBul7.body.velocity.y = -212;
      bossBul7.body.velocity.x = -212;
      var bossBul8 = this.bullet_enemy.getFirstExists(false);
      bossBul8.reset(Boss.x+10,Boss.y-10);
      bossBul8.body.velocity.y = -212;
      bossBul8.body.velocity.x = 212;
    }
  },

  enemyFire:function(){
    this.enemies.forEach(enemy=>{
      //console.log(enemy.enemyNextFire);
      if(enemy.alive && enemy.enemyNextFire<game.time.now){
        //console.log("QAQ");
        enemy.enemyNextFire = game.time.now + enemy.enemyFireDelay;
        var enemyBullet = this.bullet_enemy.getFirstExists(false);
        enemyBullet.reset(enemy.x,enemy.y+20);
        if(stage == 1){
          enemyBullet.body.velocity.y = +300;
        }
        else if(stage == 2){
          game.physics.arcade.moveToObject(enemyBullet,this.player,300);
        }
      }
    });
  },

  createEnemy:function(){
    if(stage == 1){
      if (this.nextEnemy<game.time.now && this.enemies.countDead()>0) {
        this.nextEnemy = game.time.now + this.enemyDelay;
        var enemy = this.enemies.getFirstExists(false);
        enemy.reset(game.rnd.integerInRange(20, game_width-20), 0);
        enemy.body.velocity.y = game.rnd.integerInRange(30, 60);
        enemy.enemyFireDelay = game.rnd.integerInRange(700, 1100);
        enemy.drop = game.rnd.integerInRange(0,100);
        enemy.enemyNextFire = 0;
        enemy.play('move');
      }
    }
    else if(stage == 2){
      if(this.enemyStage2<game.time.now && this.enemies.countDead()>0){
        this.enemyStage2 = game.time.now + this.enemyStage2Delay;
        var pos = game.rnd.integerInRange(20,game_width/2-20);
        var vel_y = game.rnd.integerInRange(60,120);
        var vel_x = game.rnd.integerInRange(60,80);
        var firedelay = game.rnd.integerInRange(900,1500);
        var enemy_1 = this.enemies.getFirstExists(false);
        enemy_1.reset(pos,0);
        var enemy_2 = this.enemies.getFirstExists(false);
        enemy_2.reset(game_width-pos,0);
        enemy_1.body.velocity.y = vel_y;
        enemy_1.body.velocity.x = vel_x;
        enemy_1.enemyFireDelay = firedelay;
        enemy_1.enemyNextFire = 0;
        enemy_1.drop = game.rnd.integerInRange(0,100);
        enemy_1.play('move');
        enemy_2.body.velocity.y = vel_y;
        enemy_2.body.velocity.x = -1*vel_x;
        enemy_2.enemyFireDelay = firedelay;
        enemy_2.enemyNextFire = 0;
        enemy_2.drop = game.rnd.integerInRange(0,100);
        enemy_2.play('move');
      }
    }
  },

  stage3:function(){
    if(!boss_reset){
      Boss = this.boss.getFirstExists(false);
      Boss.reset(game_width/2,0);
      Boss.play('bossAnimation');
      this.boss.moveCD = game.time.now + 4000;
      boss_reset = true;
      Boss.body.velocity.x = 250;
    }
    else{
      if(this.boss.moveCD < game.time.now){
        this.boss.moveCD = game.time.now + this.boss.moveDelay;
        game.physics.arcade.moveToObject(Boss,this.player,250);
      }
      else{
        if(Boss.body.x == 499){
          Boss.body.velocity.x = -250;
          this.boss.way = false;
        }
        else if(Boss.body.x == 0){
          Boss.body.velocity.x = 250;
          this.boss.way = true;
        }
    
        if(Boss.body.y == 408){
          Boss.body.velocity.y = -250;
          Boss.body.velocity.x = 0;
        }
        else if(Boss.body.y == 0 && Boss.body.velocity.y == 0){
          if(this.boss.way){
            Boss.body.velocity.x = 250;
          }
          else{
            Boss.body.velocity.x = -250;
          }
        }
      }
    }
  },

  playerHitEnemies:function(player,enemy){
    player.kill();
    var item = this.dropItem(enemy.drop);
    enemy.kill();
    if(item){
      item.reset(enemy.x,enemy.y);
      item.body.velocity.y = 100;
    }
    var enemyDie = game.add.sprite(enemy.x-20,enemy.y-20,'eneExp');
    enemyDie.animations.add('Die',[0,1,2,3,4,5],16,false);
    enemyDie.animations.play('Die',null,false,true);
    var playerDie = game.add.sprite(player.x-20,player.y-20,'eneExp');
    playerDie.animations.add('Die',[0,1,2,3,4,5],16,false);
    playerDie.animations.play('Die',null,false,true);
    if(life>0){
      this.loseHeart();
      life--;
      this.player.reset(game_width/2,game.height);
      this.invincibleTween.start().repeat(5);
    }
    else{
      game.state.start('end');
    }
  },

  bulletsHitEnemies:function(bullet,enemy){
    bullet.kill();
    var item = this.dropItem(enemy.drop);
    enemy.kill();
    if(item){
      item.reset(enemy.x,enemy.y);
      item.body.velocity.y = 100;
    }
    var enemyDie = game.add.sprite(enemy.x-20,enemy.y-20,'eneExp');
    enemyDie.animations.add('Die',[0,1,2,3,4,5],16,false);
    enemyDie.animations.play('Die',null,false,true);
    game.global.score += 10;
    this.changeScore();
  },

  raserHitEnemies:function(raser,enemy){
    var item = this.dropItem(enemy.drop);
    enemy.kill();
    if(item){
      item.reset(enemy.x,enemy.y);
      item.body.velocity.y = 100;
    }
    var enemyDie = game.add.sprite(enemy.x-20,enemy.y-20,'eneExp');
    enemyDie.animations.add('Die',[0,1,2,3,4,5],16,false);
    enemyDie.animations.play('Die',null,false,true);
    game.global.score += 10;
    this.changeScore();
  },

  bulletsHitBoss:function(bos_,bullet){
    bullet.kill();
    var bulletBreak = game.add.sprite(bullet.x-20,bullet.y-20,'eneExp');
    bulletBreak.animations.add('Die',[0,1,2,3,4,5],16,false);
    bulletBreak.animations.play('Die',null,false,true);
    this.boss.Hp -= 1;
    if(this.boss.Hp <0){
      game.global.FightBoss = true;
      this.emitter.x = bos_.x;
      this.emitter.y = bos_.y;
      this.emitter.start(true,800,null,15);
      bos_.kill();
      game.time.events.add(1000,function(){game.state.start('end');});
    }
  },

  raserHitBoss:function(bos_,bullet){
    var bulletBreak = game.add.sprite(bullet.x-20,bullet.y-20,'eneExp');
    bulletBreak.animations.add('Die',[0,1,2,3,4,5],16,false);
    bulletBreak.animations.play('Die',null,false,true);
    this.boss.Hp -= 1;
    if(this.boss.Hp <0){
      game.global.FightBoss = true;
      this.emitter.x = bos_.x;
      this.emitter.y = bos_.y;
      this.emitter.start(true,800,null,15);
      bos_.kill();
      game.time.events.add(1000,function(){game.state.start('end');});
    }
  },

  bullet_enemyHitPlayer:function(bullet_enemy,player){
    player.kill();
    bullet_enemy.kill();
    var playerDie = game.add.sprite(player.x-20,player.y-20,'eneExp');
    playerDie.animations.add('Die',[0,1,2,3,4,5],16,false);
    playerDie.animations.play('Die',null,false,true);
    if(life>0){
      this.loseHeart();
      life--;
      this.player.reset(game_width/2,game.height);
      this.invincibleTween.start().repeat(5);
    }
    else{
      game.state.start('end');
    }
  },

  bullet_enemyHitBullets:function(bullet_enemy,bullets){
    bullets.kill();
    console.log("qAQ");
    //bullet_enemy.kill();
  },

  BossHitPlayer:function(boss_,player){
    player.kill();
    var playerDie = game.add.sprite(player.x-20,player.y-20,'eneExp');
    playerDie.animations.add('Die',[0,1,2,3,4,5],16,false);
    playerDie.animations.play('Die',null,false,true);
    if(life>0){
      this.loseHeart();
      life--;
      this.player.reset(game_width/2,game.height);
      this.invincibleTween.start().repeat(5);
    }
    else{
      game.state.start('end');
    }
  },

  changeScore:function(){
    var score_string = String(game.global.score);
    var string_long = score_string.length; 
    if(game.global.score >= 1000){level = 3;}
    else if(game.global.score >= 300){level = 2;}

    if(game.global.score >= 500 && stage == 1){
      stage = 2;
      var next_stage2 = game.add.text(game_width/2-50,game.height,'Stage : 2',{font:'34px Arial',fill:'#fff'});
      var textTween2 = game.add.tween(next_stage2.position).to({y:-100},1200);
      textTween2.start();
    }
    else if(game.global.score >= 1200 && stage == 2){
      stage = 3;
      var next_stage3 = game.add.text(game_width/2-50,game.height,'Stage : 3',{font:'34px Arial',fill:'#fff'});
      var textTween3 = game.add.tween(next_stage3.position).to({y:-100},1200);
      textTween3.start();
    }
    for(i=0;i<string_long;i++){
      if(score_display[i]){
        score_display[i].kill();
      }
      switch(score_string[i]){
        case '0':
          score_display[i] = game.add.sprite(game_width + 110 + i*32,0,'number',0);
        break;
        case '1':
          score_display[i] = game.add.sprite(game_width + 110 + i*32,0,'number',1);
        break;
        case '2':
          score_display[i] = game.add.sprite(game_width + 110 + i*32,0,'number',2);
        break;
        case '3':
          score_display[i] = game.add.sprite(game_width + 110 + i*32,0,'number',3);
        break;
        case '4':
          score_display[i] = game.add.sprite(game_width + 110 + i*32,0,'number',4);
        break;
        case '5':
          score_display[i] = game.add.sprite(game_width + 110 + i*32,0,'number',5);
        break;
        case '6':
          score_display[i] = game.add.sprite(game_width + 110 + i*32,0,'number',6);
        break;
        case '7':
          score_display[i] = game.add.sprite(game_width + 110 + i*32,0,'number',7);
        break;
        case '8':
          score_display[i] = game.add.sprite(game_width + 110 + i*32,0,'number',8);
        break;
        case '9':
          score_display[i] = game.add.sprite(game_width + 110 + i*32,0,'number',9);
        break;
      }
    }
  },

  fire:function(){
    if(game.input.keyboard.isDown(Phaser.Keyboard.Z)){
      if(!this.player.alive || this.nextShot > game.time.now){
        return;
      }
      if(this.bullets.countDead() == 0){
        return;
      }
      this.nextShot = game.time.now + this.shotDelay;
      this.shotAudio.play();
      if(level == 1){
        var bullet = this.bullets.getFirstExists(false);
        bullet.reset(this.player.x,this.player.y-20);
        bullet.body.velocity.y = -500;
      }
      else if(level == 2){
        var bullet1 = this.bullets.getFirstExists(false);
        bullet1.reset(this.player.x-10,this.player.y-20);
        var bullet2 = this.bullets.getFirstExists(false);
        bullet2.reset(this.player.x+10,this.player.y-20);        
        bullet1.body.velocity.y = -500;
        bullet2.body.velocity.y = -500;
      }
      else if(level == 3){
        var bullet1 = this.bullets.getFirstExists(false);
        bullet1.reset(this.player.x-10,this.player.y-20);
        bullet1.body.velocity.y = -500;
        var bullet2 = this.bullets.getFirstExists(false);
        bullet2.reset(this.player.x+10,this.player.y-20);        
        bullet2.body.velocity.y = -500;
        var bullet3 = this.bullets.getFirstExists(false);
        bullet3.reset(this.player.x-15,this.player.y-15);
        bullet3.body.velocity.y = -354;
        bullet3.body.velocity.x = -300;
        var bullet4 = this.bullets.getFirstExists(false);
        bullet4.reset(this.player.x+15,this.player.y-15);
        bullet4.body.velocity.y = -354;
        bullet4.body.velocity.x = 300;
      }
    }
  },

  loseHeart:function(){
    life_display[life].kill();
    life_display[life] = game.add.image(game_width + 80 + life*26,60,'darkHeart');
  },

  movePlayer:function(){
    if(this.cursor.left.isDown){
      this.player.body.x = this.player.body.x-5;
    }
    if(this.cursor.right.isDown){
      this.player.body.x = this.player.body.x+5;
    }
    if(this.cursor.up.isDown){
      this.player.body.y = this.player.body.y-5;
    }
    if(this.cursor.down.isDown){
      this.player.body.y = this.player.body.y+5;
    }
  }
};

var game = new Phaser.Game(game_width + ui_width,500,Phaser.AUTO,'canvas');
game.global = {
  score : 0,
  sound : false,
  FightBoss : false,
  bgm : null
 }
game.state.add('boot',bootState);
game.state.add('load',loadState);
game.state.add('main',mainstate);
game.state.add('menu',menuState);
game.state.add('control',controlState);
game.state.add('end',endState);
game.state.add('leader',leaderState);
//game.state.add('multi',multipleState);
game.state.start('boot');
