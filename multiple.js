var multipleState = {
  preload:function(){

  },
  create:function(){
    //init
    level = 1;
    stage = 1;
    life = 2;
    game.global.score = 0;
    boss_reset = false;

    //game.stage.backgroundColor = '#696969';
    this.sea = game.add.tileSprite(0,0,game_width,500,'sea');
    this.scoreTxt = game.add.text(game_width,0,'Score : ',{font:'34px Arial',fill:'#fff'});
    this.lifeTxt = game.add.text(game_width,50,'Life : ',{font:'34px Arial',fill:'#fff'});
    for(i=0;i<3;i++){
      life_display[i] = game.add.image(game_width + 80 + i*26,60,'heart');
    }
    game.world.setBounds(0,0,game_width-5,500);
    //game.physics.startSystem(Phaser.Physics.ARCADE);
    //keyboard
    this.cursor = game.input.keyboard.createCursorKeys();
    //player
    this.player = game.add.sprite(game_width/2,game.height,'player');
    this.player.anchor.setTo(0.5,0.5);
    this.player.animations.add('normal',[0,1],8,true);
    
    game.physics.arcade.enable(this.player);
    this.player.body.collideWorldBounds = true;
    this.player.body.setSize(23,32,4,0);
    //drop score
    this.dropScore = game.add.group();
    this.dropScore.enableBody = true;
    this.dropScore.createMultiple(100,'coin');
    this.dropScore.setAll('anchor.x',0.5);
    this.dropScore.setAll('anchor.y',0.5);
    this.dropScore.setAll('outOfBoundsKill',true);
    this.dropScore.setAll('checkWorldBounds',true);
    //drop heart
    this.dropHeart = game.add.group();
    this.dropHeart.enableBody = true;
    this.dropHeart.createMultiple(100,'dropHeart');
    this.dropHeart.setAll('anchor.x',0.5);
    this.dropHeart.setAll('anchor.y',0.5);
    this.dropHeart.setAll('outOfBoundsKill',true);
    this.dropHeart.setAll('checkWorldBounds',true);
    //drop aim
    this.dropAim = game.add.group();
    this.dropAim.enableBody = true;
    this.dropAim.createMultiple(100,'aim');
    this.dropAim.setAll('anchor.x',0.5);
    this.dropAim.setAll('anchor.y',0.5);
    this.dropAim.setAll('outOfBoundsKill',true);
    this.dropAim.setAll('checkWorldBounds',true);
    //aiming bullet
    this.aim = game.add.group();
    this.aim.enableBody = true;
    this.aim.createMultiple(100,'aimbullet');
    this.aim.setAll('anchor.x',0.5);
    this.aim.setAll('anchor.y',0.5);
    this.aim.setAll('outOfBoundsKill',true);
    this.aim.setAll('checkWorldBounds',true);
    this.aimlevel = 0;
    this.aimshotCD = 0;
    this.aimshotDelay1 = 2000;
    this.aimshotDelay2 = 1500;
    //bullets
    this.bullets = game.add.group();
    this.bullets.enableBody = true;
    this.bullets.createMultiple(100,'bullet');
    this.bullets.setAll('anchor.x',0.5);
    this.bullets.setAll('anchor.y',0.5);
    this.bullets.setAll('outOfBoundsKill', true);
    this.bullets.setAll('checkWorldBounds', true);
    this.nextShot = 0;
    this.shotDelay = 100; 
    this.shotAudio = game.add.audio('shoot');
    this.shotAudio.volume = 0.5;
    //bullets_enemy
    this.bullet_enemy = game.add.group();
    this.bullet_enemy.enableBody = true;
    this.bullet_enemy.createMultiple(100,'bullet_enemy');
    this.bullet_enemy.setAll('anchor.x',0.5);
    this.bullet_enemy.setAll('anchor.y',0.5);
    this.bullet_enemy.setAll('outOfBoundsKill',true);
    this.bullet_enemy.setAll('checkWorldBounds',true);
    //enemies
    this.enemies = game.add.group();
    this.enemies.enableBody = true;
    this.enemies.createMultiple(50,'enemy');
    this.enemies.setAll('anchor.x',0.5);
    this.enemies.setAll('anchor.y',0.5);
    this.enemies.setAll('outOfBoundsKill',true);
    this.enemies.setAll('checkWorldBounds',true);
    this.enemies.setAll('body.width',23);
    this.enemies.setAll('body.offset.x',4);
    this.enemies.setAll('drop',0);
    this.enemies.forEach(enemy => {
      enemy.animations.add('move',[0,1],8,true);
    });
    this.nextEnemy = 0;//stage1
    this.enemyDelay = 500;//stage1
    this.enemyStage2 = 0;
    this.enemyStage2Delay = 700;
    //boss
    this.boss = game.add.group();
    this.boss.enableBody = true;
    this.boss.createMultiple(1,'boss');
    this.boss.setAll('anchor.x',0.5);
    this.boss.setAll('anchor.y',0.5);
    this.boss.setAll('checkWorldBounds',true);
    this.boss.setAll('body.collideWorldBounds',true);
    this.boss.forEach(theBoss=>{
      theBoss.animations.add('bossAnimation',[0,1,2],4,true);
    })
    this.boss.way = true;
    this.boss.moveCD = 0;
    this.boss.moveDelay = 7000;
    this.boss.fireCD = 0;
    this.boss.fireDelay = 500;
    this.boss.Hp = 1000;
    //skill-flash
    this.Flash = game.add.sprite(game_width+20,game.height-90,'flash');
    this.Flash.animations.add('skill_flash',[1,2,3,4],1,false);
    //skill-Invincible
    this.Invincible = game.add.sprite(game_width+94,game.height-90,'inivincible_img');
    this.Invincible.animations.add('inivincible',[1,2,3,4,5,6,7,8,9],1,false);
    this.invincibleTween = game.add.tween(this.player).to({alpha:0},250).yoyo(true);
    //skill-ultimate
    this.Ultimate = game.add.sprite(game_width+168,game.height-90,'Ult');
    this.Ultimate.animations.add('Ultimate',[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18],1,false);
    this.Bomb =game.add.group();
    this.Bomb.enableBody = true;
    this.Bomb.createMultiple(1,'bomb');
    this.Bomb.setAll('anchor.x',0.5);
    this.Bomb.setAll('anchor.y',0.5);
    this.Bomb.setAll('checkWorldBounds',true);
    this.Bomb.setAll('outOfBoundsKill',true);
    this.bomb;
    //boss emitter
    this.emitter = game.add.emitter(0, 0,15);
    this.emitter.makeParticles('pixel');
    this.emitter.setYSpeed(-150,150);
    this.emitter.setXSpeed(-150,150);
    this.emitter.setScale(2,0,2,0,800);
    this.emitter.gravity = 0;
    this.keyV = game.input.keyboard.addKey(Phaser.Keyboard.V);
    this.keyQ = game.input.keyboard.addKey(Phaser.Keyboard.Q);
    this.Qbutton = game.add.image(game_width+20,game.height/2-50,'Q_up');
    this.keyW = game.input.keyboard.addKey(Phaser.Keyboard.W);
    this.Wbutton = game.add.image(game_width+160,game.height/2-50,'W_down');
    this.keyE = game.input.keyboard.addKey(Phaser.Keyboard.E);
    this.Ebutton = game.add.image(game_width+220,game.height/2-50,'E');
    this.shotText = game.add.text(game_width+105,game.height/2-25,'Voice',{font:'30px Arial',fill:"#000000"});
    this.shotText.anchor.setTo(0.5,0.5);

    this.keyA = game.input.keyboard.addKey(Phaser.Keyboard.A);
    this.Abutton = game.add.image(game_width+20,game.height/2+20,'A_up');
    this.keyS = game.input.keyboard.addKey(Phaser.Keyboard.S);
    this.Sbutton = game.add.image(game_width+160,game.height/2+20,'S_down');
    this.keyD = game.input.keyboard.addKey(Phaser.Keyboard.D);
    this.Dbutton = game.add.image(game_width+220,game.height/2+20,'D');
    this.BGMText = game.add.text(game_width+105,game.height/2+45,'BGM',{font:'30px Arial',fill:"#000000"});
    this.BGMText.anchor.setTo(0.5,0.5);

    this.keyVFlag = false;
    this.player.animations.play('normal');
    this.changeScore();
  },
}