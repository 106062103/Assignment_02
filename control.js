var controlState = {
  preload:function(){

  },

  create:function(){
    this.back = game.add.tileSprite(0,0,game.width,game.height,'sea');

    var title = game.add.text(game.width/2,100,'↑/↓/←/→  Control the plane',{font:'30px Arial',fill:'#ffffff'});
    title.anchor.setTo(0.5,0.5);
    var z_ = game.add.text(game.width/2,150,'Z  to fire the bullets',{font:'30px Arial',fill:'#ffffff'});
    z_.anchor.setTo(0.5,0.5);
    var x_ = game.add.text(game.width/2,200,'↑/↓/←/→ + X  to use the "Flash" skill',{font:'30px Arial',fill:'#ffffff'});
    x_.anchor.setTo(0.5,0.5);
    var c_ = game.add.text(game.width/2,250,'C  to use the "Invincible" skill (avoid any attack)',{font:'30px Arial',fill:'#ffffff'});
    c_.anchor.setTo(0.5,0.5);
    var v_ = game.add.text(game.width/2,300,'V  to use the "Explosion" skill',{font:'30px Arial',fill:'#ffffff'});
    v_.anchor.setTo(0.5,0.5);
    var v_1 = game.add.text(game.width/2,350,'Press  V  again to detonate the bomb',{font:'30px Arial',fill:'#ffffff'});
    v_1.anchor.setTo(0.5,0.5);
    var zz = game.add.text(game.width/2,400,'Press  Z  back to menu',{font:'16px Arial',fill:'#ffffff'});
    zz.anchor.setTo(0.5,0.5);
    this.littletextTween = game.add.tween(zz).to({alpha:0},500).yoyo(true);
    this.littletextTween.start().repeat();
    this.keyZ = game.input.keyboard.addKey(Phaser.Keyboard.Z);
  },

  update:function(){
    this.back.tilePosition.y+=1;
    this.keyZ.onDown.add(this.backToMenu,this);
  },

  backToMenu:function(){
    game.state.start('menu');
  }
}
