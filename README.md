# Software Studio 2019 Spring Assignment 2
## Notice
* Replace all [xxxx] to your answer

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Complete game process|15%|Y|
|Basic rules|20%|Y|
|Jucify mechanisms|15%|Y|
|Animations|10%|Y|
|Particle Systems|10%|Y|
|UI|5%|Y|
|Sound effects|5%|Y|
|Leaderboard|5%|Y|

## Website Detail Description

# Basic Components Description : 
1. Jucify mechanisms : 
    >關卡：<br>
    >>第一關是0~500分，敵方只會隨機生成，以直線往下前進，並且朝行進方向發射子彈<br><br>
    第二關是500~1200分，敵方會成雙成對生成，以斜線方向前進，並且會朝玩家所在位置發射子彈，子彈與敵人行進速度都會比第一關還要快<br><br>
    第三關(Boss)，大部分時間會在最上排左右移動，並且朝八個方向發射子彈，有時候會朝玩家衝過去，用身體撞玩家<br>

    >等級：<br>
    >>1等只會朝正前方射出一顆子彈<br>
    2等會朝正前方射出兩顆子彈<br>
    3等會朝前方射出兩顆子彈，且會斜向各射出一顆子彈<br>
    當分數達300,1000時會自動升級

    >技能：<br>
    >有三個技能可以使用，分別有各自的冷卻時間，可以從UI上的圖示得知是否可以使用
    >>閃現：方向鍵搭配X鍵即可朝特定方向閃現一段距離<br><br>
    隱身：C鍵可以使用隱身技能，隱身其間不會被敵方碰觸到或是攻擊到<br><br>
    遙控炸彈：V鍵可以朝前方放出一顆炸彈，再按一次V鍵可以引爆炸彈，摧毀爆炸範圍內的敵方與敵方子彈<br>

2. Animations : 
    >玩家、敵方、Boss皆有動畫<br>
    被子彈打到也會有爆炸動畫產生<br>
    而技能冷卻時間圖示亦是用動畫製成的
3. Particle Systems : 
    >摧毀Boss的時候會有Partixle Explosion效果
4. Sound effects : 
    >BGM
    >>可以藉由A鍵增強BGM音量，S鍵降低BGM音量，D鍵使BGM禁音

    >Shooting
    >>當玩家射擊時會有射擊音效，Q鍵可以增強音效音量，W鍵可以降低音效音量，E鍵可以使音效禁音
5. Leaderboard : 
    >會在database中紀錄歷史最高分
    <br>並在LeaderBoard中顯示

# Bonus Functions Description : 
1. Boss : 
    >在「關卡」之中已介紹
2. 掉落物 : 
    >擊倒敵方會有機率掉落物品，一共有四種掉落物
    
    >金幣：可以增加分數20分

    >愛心：可以增加一顆愛心(命)，最多只能儲存三顆

    >綠色能力之星：可以獲得自動瞄準武器(下面詳細介紹)，共有三個等級，每獲得一顆升一等

    >藍色能力之星：可以呼叫幫手5秒鐘(下面詳細介紹)
3. 特殊攻擊模式 :
    >遙控炸彈：
    >>為技能之一，可以大範圍炸毀敵軍還有敵軍子彈，且對Boss造成大量傷害

    >自動瞄準：
    >>會自動瞄準最近的敵軍，共有三個等級
    <br>第一等級：一段時間後自動射出一顆子彈
    <br>第二等級：更快的射出一顆子彈
    <br>第三等級：一次射出兩顆自動瞄準子彈

    >幫手：
    >>呼叫兩個幫手附在玩家兩側，會跟著玩家移動，並且自動攻擊
    <br>攻擊模式為雷射，可以一整排的催毀敵軍還有敵軍子彈
