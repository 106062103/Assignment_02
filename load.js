
var loadState ={
  preload:function(){
    var progress = game.add.sprite(game.width/2,200,'progressBar');
    progress.anchor.setTo(0.5,0.5);
    game.load.setPreloadSprite(progress);
    //
    game.load.spritesheet('number','asset/number.png',32,40);
    game.load.spritesheet('player','asset/pipo-airship02.png',32,32);
    game.load.spritesheet('enemy','asset/pipo-airship01.png',32,32);
    game.load.spritesheet('inivincible_img','asset/Flash.png',64,64);
    game.load.spritesheet('boss','asset/boss.png',96,92);
    game.load.spritesheet('flash','asset/skill_all.png',64,64);
    game.load.spritesheet('Ult','asset/ultimateQAQ.png',64,64);
    game.load.spritesheet('exp','asset/explosion1.png',240,240);
    game.load.spritesheet('eneExp','asset/eneExp.png',40,40);
    game.load.spritesheet('raser','asset/bullet22.png',30,800);
    game.load.image('helper','asset/helper.png');
    game.load.image('Q_up','asset/Q_up.png');
    game.load.image('A_up','asset/A_up.png');
    game.load.image('W_down','asset/W_down.png');
    game.load.image('S_down','asset/S_down.png');
    game.load.image('E','asset/audioE.png');
    game.load.image('E_','asset/audioE_.png');
    game.load.image('D','asset/audioD.png');
    game.load.image('D_','asset/audioD_.png');
    game.load.image('pixel','asset/pixel.png')
    game.load.image('bullet','asset/bullet.png');
    game.load.image('bullet_enemy','asset/bullet_enemy.png');
    game.load.image('sea','asset/sea.png');
    game.load.image('heart','asset/sticon3b-3.png');
    game.load.image('darkHeart','asset/sticon3j-3.png');
    game.load.image('bomb','asset/bullet10.png');
    game.load.image('aimbullet','asset/bullet46.png');
    game.load.image('dropHeart','asset/sticon3b-2.png');
    game.load.image('coin','asset/coin.png');
    game.load.image('aim','asset/aim.png');
    game.load.image('special','asset/sticon4c-3.png');
    //
    game.load.audio('shoot','asset/laser4.mp3');
    game.load.audio('bgm','asset/bgm.mp3');
    //
    game.load.image('choice1','asset/choice1.png');
    game.load.image('choice2','asset/choice2.png');
  },

  create:function(){
    game.state.start('menu');
  }

}