var endState = {
  preload:function(){

  },

  create:function(){
    this.back = game.add.tileSprite(0,0,game.width,game.height,'sea');
    if(game.global.FightBoss){
      var good = game.add.text(game.width/2,150,'Congratulations',{font:'45px Arial',fill:"#ffffff"});
      good.anchor.setTo(0.5,0.5);
    }
    else{
      var lose = game.add.text(game.width/2,150,'Oh....So Sad',{font:'45px Arial',fill:"#ffffff"});
      lose.anchor.setTo(0.5,0.5);
    }
    var score = game.add.text(game.width/2,250,'Your Score is '+game.global.score,{font:'30px Arial',fill:"#ffffff"});
    score.anchor.setTo(0.5,0.5);
    var zzz = game.add.text(game.width/2,400,'Press  Z  back to menu',{font:'16px Arial',fill:"#ffffff"});
    zzz.anchor.setTo(0.5,0.5);
    this.littletextTween = game.add.tween(zzz).to({alpha:0},500).yoyo(true);
    this.littletextTween.start().repeat();
    this.keyZ = game.input.keyboard.addKey(Phaser.Keyboard.Z);
    var text_ref = firebase.database().ref('HighestScore');
    var text = firebase.database().ref('HighestScore').once('value').then((data)=>{
      console.log(data.val());
      HighestScores = data.val().HighestScore;
      if(game.global.score>HighestScores){
        text_ref.set({HighestScore:game.global.score});
      }
    });
  },

  update:function(){
    this.back.tilePosition.y += 1;
    this.keyZ.onDown.add(this.backMenu,this);
  },

  backMenu:function(){
    game.state.start('menu');
  }
}