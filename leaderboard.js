var leaderState = {
  preload:function(){

  },

  create:function(){
    this.back = game.add.tileSprite(0,0,game.width,game.height,'sea');
    this.keyZ = game.input.keyboard.addKey(Phaser.Keyboard.Z);
    var littletext = game.add.text(game.width/2,400,'Press Z to go back',{font:'20px Arial',fill:"#ffffff"})
    littletext.anchor.setTo(0.5,0.5);
    this.littletextTween = game.add.tween(littletext).to({alpha:0},500).yoyo(true);
    this.littletextTween.start().repeat();
    var HighestScores;
    var text_ref = firebase.database().ref('HighestScore').once('value').then((data)=>{
      console.log(data.val());
      HighestScores = data.val().HighestScore;
      this.HighScore = game.add.text(game.width/2,150,'Highest Score : ' + HighestScores,{font:'50px Arial',fill:"#ffffff"});
      this.HighScore.anchor.setTo(0.5,0.5);
    });
  },

  update:function(){
    this.back.tilePosition.y += 1;
    this.keyZ.onDown.add(this.goBack,this);
  },

  goBack:function(){
    game.state.start('menu');
  }
}